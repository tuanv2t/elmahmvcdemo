﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ElmahMVCDemo.Startup))]
namespace ElmahMVCDemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
